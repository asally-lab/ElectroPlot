## Electroplot.py
import os
from tifffile import TiffFile
from tkinter import filedialog
import numpy as np
from Functions.dataGrabber import grabMetadata, grabTifdata, grabImageJdata, grabData, tif2png
from Functions.masks import logTransform
from matplotlib import pyplot as plt
from Functions.masks import applyMask

import moviepy.editor as mp
import Functions.Arranger as ar

# Initialise the data to be analysed. 
# example: condition1 = ep.Dataset('Control', imagetype = 'image')
class Dataset(object):
    def __init__(self, name, path = None, imagetype = 'stack'): 
<<<<<<< HEAD
        self.name = name        #the name of the dataset
        self.raw = []           #raw data in dataset including metadata and image data in form of array
        self.data = []          #data within the dataset, stored as n arrays for n images
        self.type = imagetype   #the type of dataset, currently can be one image or entire stack 
  
        #open the dataset, if no path to the data, then a dialog prompt is called. One file or an entire directory may be selected based on self.type
=======
        self.name = name
        self.base = []
        self.raw = []
        self.stack = []
        self.data = []
        self.type = imagetype
        self.base = []

        
>>>>>>> 5d035e7307263e906b8aca9ce4a19c439396e6ac
        if path == None: 
            if self.type == 'stack': 
                self.path = filedialog.askdirectory()
            else: 
                self.path = filedialog.askopenfilename()
        else:
            self.path = path 

        if self.type == 'stack':
            for file in os.listdir(self.path):
                try:
                    self.raw = grabData(str(self.path + '\\' + file))
<<<<<<< HEAD
                    try:
                        self.metadata = self.raw['ImageJ']                  #seperates imagej metadata into metadata
                    except TypeError:                                       #if no metadata present skips this
                        pass
                    self.data.append(self.raw['Pixels'])                    #add pixel data for image (file), into data array
                    #self.base.append(grabTifdata(self.path + '\\' +file)) 
                except TypeError:
                    print(self.raw)                                         #debug print if image load fails         
        elif self.type == 'image':
            self.raw = grabData(str(self.path))                             #
            self.data.append(self.raw['Pixels'])
            try:
                self.metadata = self.raw['ImageJ']          
            except TypeError:
                pass                                                     
        
=======
                    self.metadata = self.raw['ImageJ']
                    self.data.append(self.raw['Pixels'])
                   # self.base.append(grabTifdata(self.path + '\\' +file))             
        else:
            self.raw = grabData(str(self.path))
            self.data.append(self.raw['Pixels'])
            self.metadata = self.raw['ImageJ']
            #self.base = grabTifdata(self.path)
>>>>>>> 5d035e7307263e906b8aca9ce4a19c439396e6ac

    def mask(self, threshold, masktype = 'edges', show = False): 
        if self.type == 'stack':
            for imagearray in self.data:
                masked = applyMask(imagearray, threshold, masktype, show) 
                
                #image = masked 
        else:
<<<<<<< HEAD
            masked = applyMask(self.data, threshold, masktype, show)
            self.data = masked
            
=======
            masked = applyMask(self.path, threshold, masktype, show)
            self.data = masked
>>>>>>> 5d035e7307263e906b8aca9ce4a19c439396e6ac
    def transform(self, transformer, *args):
        if self.type == 'stack':
            for i in self.data:
                output = transformer(i, *args)
        elif self.type == 'image':
            output = transformer(self.data, *args)
        return output
    
    def getZprofile(self, plot = True, mask = None):
        zprofile = []
        #i = 0
        if self.type == 'stack': 
            if mask == None: 
                for image in self.data: 
                    zprofile.append(np.average(image))
            elif mask == 'log':
                    zprofile.append(logTransform(self.data, 10))
            else: 
                print('bork')
                    #process(self.data['Pixels'], **kwargs           
        else: 
            print("getZprofile: This dataset is a" + str(self.type) + ", not a stack!")
            
        if plot == True:
            plot = plt.plot(zprofile)
            #plt.show()
            zprofileplot = plt.savefig('zprofile.png')
            return zprofileplot
        else:
            return zprofile

class Figure(): 
    def __init__(self, name, *datasets, rows = None, columns = None, duration = None ):
        #initialise containers for subfigure objects and positional array
        subfigures = []                                         
        positions = []
        total = int
        
        #automatically calculate rows and columns if none specified
        if rows == None:
            rows = len(datasets) 
        if columns == None: 
            columns = 1
        if total != rows * columns:
            Exception('the total number of subfigures is out of bounds!')
        
        #create subfigures from datasets, and append to array
        for i in datasets:
            subfigure = ar.subFigure(i, i.name, 
                                i.data, 
                                i.metadata,
                                )
            subfigures.append(subfigure)
        self.subfigures = np.array(subfigures)
        
        #figure parameter dictionary (base)
        self.figureParams = {   'name' : name,
                                'total': int(len(datasets)),
                                'rows' : rows,
                                'cols' : columns,
                                'duration' : duration,
                                'valid' : 0
                            }
        #set positional layout for subfigures, labelled 0 - 1 left to right, rows -> columns
        c = 0
        for i in subfigures:
            i.position = c
            c+=1
            positions.append(i.position)
        positions = np.asarray(positions)
        self.figureParams['positions'] = np.array(positions.reshape(self.figureParams['rows'], self.figureParams['cols']))
        #print("Sucessfully made subfigures, access datasets " + str(subfigures.names))
        
    #arrange the layout of subfigures.
    def arrange(self, cols, rows): #arranges the grid to accomodate for electrodes with 
        self.figureParams['rows'] = rows
        self.figureParams['cols'] = cols
        if cols + rows != self.figureParams['total']:
            raise Exception('the number of columns and rows must equal the total number of datasets!')
        else:
            #self.layout = self.subfigures.reshape(self.figureParams['rows'], self.figureParams['cols'])
            self.figureParams['positions']=self.figureParams['positions'].reshape(self.figureParams['rows'], self.figureParams['cols'])
            return print ('Set figure to have ' + str(cols) + ' columns, and ' + str(rows) + ' rows:' ), print(self.figureParams['positions'])
    
    #def makeClip(self, subfigure, )
    #joins two datasets into a single subfigure
    def pairSubfigures(self, subfigures):
        #pairingcode
        return None
    def setFPS(self, fps):
        self.figureParams['fps'] = fps
        return print ('fps:' + str(self.figureParams['fps']))
    #prints the current positional layout of the Figure. 
    def showlayout(self):
        print(self.figureParams['positions'])
        
    # assign figure types to subfigures. Available types are 'image', 'movie' and 'graph'
    #for graph types, animation must be specified as true or false
    # all subfigures must be assigned before a plot can be valid
    def assignSubfigures(self, *subtypesPositions, animation = None):
        c = 0
        for i in subtypesPositions:
            position = i[0]
            self.subfigures[position].subtype = i[1]
            if self.subfigures[position].subtype == 'graph':
                self.subfigures[position].animation = i[2]
        
        '''
        for i in self.subfigures:
            i.subtype = subtypesPositions[c]
            c+=1
        '''
        self.figureParams['valid'] = True
        return print(subtypesPositions)
    
    #print the type of subfigure, at specified position
    def checkSubtype(self, position):
        if self.subfigures[position].subtype == 'graph':
            return print(self.subfigures[position].subtype), print('animated: ' + str(self.subfigures[position].animation) )
        else:
            return print(self.subfigures[position].subtype)
    
    #preview the specified subfigures before figure compilation
    def previewSubfigures(self, *subFigures):
        previews = []
        for i in subFigures:
            i.makeSubfigure()
            previews.append(i.clip)
        return previews
        
    #compiles, and outputs the animated figure
    def renderFigure(self, fps, output = '.mp4', preview = True ):
        figure = []
        cliparray = []
        c = 0
        if self.figureParams['valid'] == True:
            cliparray = []
            for subfigure in self.subfigures:
                if subfigure.subtype == 'image':
                    subfigure.makeSubfigure(fps, self.figureParams['duration'])
                
                elif subfigure.subtype == 'movie':
                    subfigure.makeSubfigure(fps)
                
                elif subfigure.subtype == 'graph':
                    subfigure.makeSubfigure(fps)
                
                else: 
                    raise Exception("< subtype = " + str(subfigure.subtype) + " >, assing subtypes with Figure.assignSubtypes()")
            cliparray.append(i.clip)
            print(i)
                
            cliparray = np.asarray(cliparray)
            cliparray = np.array(cliparray.reshape(self.figureParams['rows'], self.figureParams['cols']))
            figure = mp.clips_array(cliparray)

            figure.resize(width = 480).write_videofile(self.figureParams['name'] + output, fps = fps)
            figure.resize(width=480).preview()

        else:
            raise Exception("invalid or missing figure parameters!")
        return figure
        