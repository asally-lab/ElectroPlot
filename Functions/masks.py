# masks.py -> write custom mask functions for the z-profile
import skimage
#from skimage import io
from skimage import feature
from skimage.filters import sobel
from skimage.exposure import histogram
import numpy as np
# logarithm 
from scipy import ndimage as ndi
from skimage.segmentation import watershed
import matplotlib.pyplot as plt

def applyMask(image, threshold = 1500, masktype = 'edges', show = False):
    if masktype == 'edges':
        edges = feature.canny(image/threshold)  
        mask = ndi.binary_fill_holes(edges)
        combined = mask * image
        if show == True: 
            io.imshow(combined)
            io.show()  

    elif masktype == 'region': 
        hist, hist_centers = histogram(image)
        elevation_map = sobel(image)
        markers = np.zeros_like(image)
        markers[image < 30] = 1
        markers[image > 1500] = 2 
        segmentation = watershed(elevation_map)
        segmentation = ndi.binary_fill_holes(segmentation - 1)
        if show == True: 
            io.imshow(segmentation)
            io.show()

    elif masktype == 'electrode':
        print(str(masktype))

    elif masktype == 'hyperpolarised':
        print(str(masktype))

    elif masktype == 'depolarised':
        print(str(masktype))

    else:
        raise Exception("< 'masktype = " + str(masktype) + " > \n unknown mask type, choose from 'edges', 'electrode', 'hyperpolarise', 'depolarise' ")
    
    return combined

def logTransform(stack, divframe):
    #stack = stack / stack[divframe]
    out = []
    for image in stack: 
        image = image / stack[divframe]
        out.append(np.log(image))
    
    return out
        
        
        
    